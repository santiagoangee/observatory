package observatory

/**
  * Introduced in Week 1. Represents a location on the globe.
  * @param lat Degrees of latitude, -90 ≤ lat ≤ 90
  * @param lon Degrees of longitude, -180 ≤ lon ≤ 180
  */

object Raw {
  case class Station(
      stationId: String,
      wbanId: String,
      lat: Option[Double],
      lon: Option[Double]
  )
  case class Temperature(
      stationId: Option[String],
      wbanId: Option[String],
      month: Option[Int],
      day: Option[Int],
      temperatureF: Option[Double]
  )
}

case class Location(lat: Double, lon: Double) {
  def isAntipodeOf(location: Location): Boolean =
    (-1.0d) * this.lat == location.lat && (180.0d - this.lon) == location.lon
}

/**
  * Introduced in Week 3. Represents a tiled web map tile.
  * See https://en.wikipedia.org/wiki/Tiled_web_map
  * Based on http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
  * @param x X coordinate of the tile
  * @param y Y coordinate of the tile
  * @param zoom Zoom level, 0 ≤ zoom ≤ 19
  */
case class Tile(x: Int, y: Int, zoom: Int)

/**
  * Introduced in Week 4. Represents a point on a grid composed of
  * circles of latitudes and lines of longitude.
  * @param lat Circle of latitude in degrees, -89 ≤ lat ≤ 90
  * @param lon Line of longitude in degrees, -180 ≤ lon ≤ 179
  */
case class GridLocation(lat: Int, lon: Int)

sealed abstract class Grid(
    private val gridLocations: Vector[(GridLocation, Temperature)]
) {
  def apply(lat: Int)(lon: Int): Temperature = {
    get(lat)(lon)._2
  }

  // Here lies the problem
  def get(lat: Int)(lon: Int): (GridLocation, Temperature) = {
    val i = Grid.Height / 2 - lat
    val j = lon + Grid.Width / 2
    gridLocations(Grid.Width * i + j)
  }

  def isEmpty: Boolean =
    this.gridLocations == Vector.empty[(GridLocation, Temperature)]
  def nonEmpty: Boolean = !isEmpty
}

class TemperatureGrid(
    val temperatures: Iterable[(Location, Temperature)],
    gridLocations: Vector[(GridLocation, Temperature)]
) extends Grid(gridLocations)
class AverageGrid(gridLocations: Vector[(GridLocation, Temperature)])
    extends Grid(gridLocations)
class NormalGrid(gridLocations: Vector[(GridLocation, Temperature)])
    extends Grid(gridLocations)

case object Grid {
  val MinLatitude: Int = -89
  val MaxLatitude: Int = 90

  val MinLongitude: Int = -180
  val MaxLongitude: Int = 179

  val Height: Int = 180
  val Width: Int = 360

  val DecreasingOrder = -1

}

case object TemperatureGrid {

  def apply(
      temperatures: Iterable[(Location, Temperature)],
      gridLocations: Vector[(GridLocation, Temperature)]
  ): TemperatureGrid =
    new TemperatureGrid(temperatures, gridLocations)

}

case object AverageGrid {
  def apply(gridLocations: Vector[(GridLocation, Temperature)]): AverageGrid =
    new AverageGrid(gridLocations)

  def empty: AverageGrid =
    new AverageGrid(Vector.empty[(GridLocation, Temperature)])
}

case object NormalGrid {
  def apply(gridLocations: Vector[(GridLocation, Temperature)]): NormalGrid =
    new NormalGrid(gridLocations)

  def empty: NormalGrid =
    new NormalGrid(Vector.empty[(GridLocation, Temperature)])
}

/**
  * Introduced in Week 5. Represents a point inside of a grid cell.
  * @param x X coordinate inside the cell, 0 ≤ x ≤ 1
  * @param y Y coordinate inside the cell, 0 ≤ y ≤ 1
  */
case class CellPoint(x: Double, y: Double)

/**
  * Introduced in Week 2. Represents an RGB color.
  * @param red Level of red, 0 ≤ red ≤ 255
  * @param green Level of green, 0 ≤ green ≤ 255
  * @param blue Level of blue, 0 ≤ blue ≤ 255
  */
case class Color(red: Int, green: Int, blue: Int) {
  def +(color: Color): Color = {
    val red = this.red + color.red
    val green = this.green + color.green
    val blue = this.blue + color.blue
    Color(red, green, blue)
  }

  def *(scalar: Double): Vector[Double] =
    Vector(this.red, this.green, this.blue) map (_ * scalar)
}

case class TemperatureColorInterval(
    startTemperature: Temperature,
    endTemperature: Temperature,
    startColor: Color,
    endColor: Color
)
