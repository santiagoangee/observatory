package observatory

/**
  * 4th milestone: value-added information
  */
object Manipulation extends ManipulationInterface {

  private var _temperatureGrids: Vector[TemperatureGrid] = Vector.empty[TemperatureGrid]

  private def temperatureAverage(temperatures: Iterable[Temperature]): Temperature = {
    def seqOp(a: (Double, Int), b: Temperature): (Double, Int) = (a,b) match  {
      case ((accTemperature, accNumRecords), temperature)=>
        (accTemperature + temperature, accNumRecords + 1)
    }
    def combOp(a: (Double, Int), b: (Double, Int)): (Double, Int) = (a,b) match {
      case ((accTemperature1, accNumRecords1), (accTemperature2, accNumRecords2)) =>
        (accTemperature1 + accTemperature2, accNumRecords1 + accNumRecords2)
    }
    val averageCalculation = temperatures.aggregate((0.0D, 0)) (seqOp, combOp)

    val average = averageCalculation._1 / averageCalculation._2
    average
  }


  private def precomputeTemperatureGrid(temperatures: Iterable[(Location, Temperature)]): TemperatureGrid = {
    val queryGrid = _temperatureGrids filter (grid => grid.temperatures == temperatures)
    if (queryGrid.isEmpty) {
      val gridLocations: Vector[(GridLocation, Temperature)] = (for {
          i <- Grid.MaxLatitude to Grid.MinLatitude by Grid.DecreasingOrder
          j <- Grid.MinLongitude to Grid.MaxLongitude
        } yield {
        (
          GridLocation(i, j),
            Visualization.predictTemperature(temperatures, Location(i.toDouble, j.toDouble))
        )
        }).toVector
      val temperatureGrid = TemperatureGrid(temperatures, gridLocations)
      _temperatureGrids :+= temperatureGrid
      temperatureGrid
    } else queryGrid.head
  }

  private def precomputeAverageGrid(grids: Iterable[GridLocation => Temperature]): AverageGrid = {
    val gridLocations: Vector[(GridLocation, Temperature)] = (for {
        i <- Grid.MaxLatitude to Grid.MinLatitude by Grid.DecreasingOrder
        j <- Grid.MinLongitude to Grid.MaxLongitude
      } yield {
        val yearlyTemperatures = grids.map(grid => grid(GridLocation(i,j)))
        (GridLocation(i, j), temperatureAverage(yearlyTemperatures))
      }).toVector
    AverageGrid(gridLocations)
  }

  /**
    * @param temperatures Known temperatures
    * @return A function that, given a latitude in [-89, 90] and a longitude in [-180, 179],
    *         returns the predicted temperature at this location
    */
  def makeGrid(temperatures: Iterable[(Location, Temperature)]): GridLocation => Temperature = {
    val grid = precomputeTemperatureGrid(temperatures)
    gridLocation => grid(gridLocation.lat)(gridLocation.lon)
  }

  /**
    * @param temperaturess Sequence of known temperatures over the years (each element of the collection
    *                      is a collection of pairs of location and temperature)
    * @return A function that, given a latitude and a longitude, returns the average temperature at this location
    */
  def average(temperaturess: Iterable[Iterable[(Location, Temperature)]]): GridLocation => Temperature = {
    val grids = temperaturess map (temperatures => makeGrid(temperatures))
    val averageGrid = precomputeAverageGrid(grids)
    gridLocation => averageGrid(gridLocation.lat)(gridLocation.lon)
  }

  /**
    * @param temperatures Known temperatures
    * @param normals A grid containing the “normal” temperatures
    * @return A grid containing the deviations compared to the normal temperatures
    */
  def deviation(temperatures: Iterable[(Location, Temperature)], normals: GridLocation => Temperature): GridLocation => Temperature = {
    val grid = makeGrid(temperatures)
    gridLocation => grid(gridLocation) - normals(gridLocation)
  }

}

