package observatory

import org.apache.log4j.{Level, Logger}

import java.time.LocalDate
import scala.io.Source
import cats.implicits._
import cats.effect.{IO, IOApp}
import fs2.{Stream, text}
import fs2.io.file.{FileHandle, Files}
import observatory.Raw.Station
import observatory.error.ObservatoryError
import observatory.error.ObservatoryError.{
  AttributeNotFound,
  EmptyAttribute,
  StreamFilterError
}

import java.nio.file.Paths
import org.apache.spark.sql._
import org.apache.spark.sql.types._
import org.apache.spark.rdd.RDD

import scala.util.Try

/**
  * 1st milestone: data extraction
  */
object Extraction extends ExtractionInterface {

  Logger.getLogger("org.apache.spark").setLevel(Level.WARN)
  import org.apache.spark.sql.SparkSession

  /**
    * Spark session
    */
  val spark: SparkSession = SparkSession
    .builder()
    .appName("Observatory")
    .config("spark.executor.memory", "1G")
    .config("spark.master", "local")
    .getOrCreate()

  val StationsStruct =
    StructType(
      StructField("stnId", StringType, nullable = true) ::
        StructField("wbanId", StringType, nullable = true) ::
        StructField("lat", DoubleType, nullable = true) ::
        StructField("lon", DoubleType, nullable = true) :: Nil
    )

  val TemperaturesStruct =
    StructType(
      StructField("stnId", StringType, nullable = true) ::
        StructField("wbanId", StringType, nullable = true) ::
        StructField("month", IntegerType, nullable = true) ::
        StructField("day", IntegerType, nullable = true) ::
        StructField("temperatureF", DoubleType, nullable = true) :: Nil
    )

  /**
    * Using default Encoders for Datasets
    */
  import spark.implicits._

  /**
    * @param year             Year number
    * @param stationsFile     Path of the stations resource file to use (e.g. "/stations.csv")
    * @param temperaturesFile Path of the temperatures resource file to use (e.g. "/1975.csv")
    * @return A sequence containing triplets (date, location, temperature)
    */
  def locateTemperatures(
      year: Year,
      stationsFile: String,
      temperaturesFile: String
  ): Iterable[(LocalDate, Location, Temperature)] = {
    val stationsDF = getStations(stationsFile)
    val temperaturesDF = getTemperatures(temperaturesFile)
    sparkTemperatures(year, stationsDF, temperaturesDF).collect
  }

  def locateTemperatures(
      year: Int,
      stationsFile: String,
      temperaturesFile: String
  ): IO[Vector[(LocalDate, Location, Temperature)]] = {
    val stations = getStations(stationsFile)
    val temperatures = getTemperatures(temperaturesFile)
    // app logic below
  }

  def sparkTemperatures(
      year: Int,
      stationsDF: DataFrame,
      temperaturesDF: DataFrame
  ): RDD[(LocalDate, Location, Temperature)] = {

    val stationsJoinTemperaturesDF =
      stationsDF.join(
        temperaturesDF,
        stationsDF("stnId") <=> temperaturesDF("stnId") &&
          stationsDF("wbanId") <=> temperaturesDF("wbanId")
      )

    stationsJoinTemperaturesDF.rdd.map(row => {
      val temperature: Temperature =
        fahrenheitToCelsius(row.getAs[Double]("temperatureF"))
      val location: Location =
        Location(row.getAs[Double]("lat"), row.getAs[Double]("lon"))
      val localDate: LocalDate =
        LocalDate.of(year, row.getAs[Int]("month"), row.getAs[Int]("day"))
      (localDate, location, temperature)
    })
  }

  /**
    * @param records A sequence containing triplets (date, location, temperature)
    * @return A sequence containing, for each location, the average temperature over the year.
    */
  def locationYearlyAverageRecords(
      records: Iterable[(LocalDate, Location, Temperature)]
  ): Iterable[(Location, Temperature)] =
    sparkAverageRecords(records).collect

  def sparkAverageRecords(
      records: Iterable[(LocalDate, Location, Temperature)]
  ): Dataset[(Location, Temperature)] = {
    val recordsRDD = spark.sparkContext.parallelize(records.toSeq)
    val recordsDS = recordsRDD
      .map {
        case (_, location, temperature) => (location, temperature)
      }
      .toDS
      .withColumnRenamed("_1", "location")
      .withColumnRenamed("_2", "temperature")

    recordsDS
      .groupBy("location")
      .mean("temperature")
      .as[(Location, Temperature)]
  }

  private def getRDDFromResource(resource: String): RDD[String] = {
    val fileStream = Source.getClass.getResourceAsStream(resource)
    spark.sparkContext.makeRDD(
      Source.fromInputStream(fileStream).getLines().toVector
    )
  }

  def getStationsStream(stationsFile: String): IO[Vector[Station]] = {
    def parseMandatoryAttr(
        attr: String,
        index: Int
    ): Either[EmptyAttribute, String] =
      if (attr.nonEmpty) Right(attr)
      else Left(EmptyAttribute(index))

    def parseStation(line: String): Option[Station] = {
      val stationIdIndex = 0
      val wbanIdIndex = 1
      val latIndex = 2
      val lonIndex = 3
      val attrs = line.split(",").map(attr => attr.trim)
      if (attrs.length == 4) {
        (for {
          rawStationId <- Try(attrs(stationIdIndex)).toOption.toRight(
            AttributeNotFound(stationIdIndex)
          )
          stationId <- parseMandatoryAttr(rawStationId, stationIdIndex)
          rawWbanId <- Try(attrs(wbanIdIndex)).toOption.toRight(
            AttributeNotFound(wbanIdIndex)
          )
          wbanId <- parseMandatoryAttr(rawWbanId, wbanIdIndex)
          lat = Try(attrs(latIndex).toDouble).toOption
          lon = Try(attrs(lonIndex).toDouble).toOption
        } yield Station(stationId, wbanId, lat, lon)).toOption
      } else None
    }

    val stationStream = Files[IO]
      .readAll(Paths.get("testdata/fahrenheit.txt"), 4096)
      .through(text.utf8Decode)
      .through(text.lines)
      .filter(line => line.trim.nonEmpty && !line.startsWith("//"))
      .map(line => parseStation(line))
      .filter(station => station.isDefined)
      .flatMap {
        case Some(station) => Stream(station)
        case None          => Stream.raiseError[IO](StreamFilterError)
      }
  }

  def getStations(stationsFile: String): DataFrame =
    spark.read
      .option("sep", ",")
      .schema(StationsStruct)
      .csv(getRDDFromResource(stationsFile).toDS)
      .filter($"lat".isNotNull && $"lon".isNotNull)

  def getTemperatures(temperaturesFile: String): DataFrame =
    spark.read
      .option("sep", ",")
      .schema(TemperaturesStruct)
      .csv(getRDDFromResource(temperaturesFile).toDS)
      .filter(
        ($"stnId".isNotNull || $"wbanId".isNotNull)
          && $"month".isNotNull
          && $"day".isNotNull
          && $"temperatureF".isNotNull
      )

  def fahrenheitToCelsius(temperatureFahrenheit: Double): Temperature =
    ((temperatureFahrenheit - 32.0d) * 5.0d) / 9.0d

}
