package observatory

import com.sksamuel.scrimage.{Image, Pixel}
import math.{abs, acos, cos, sin, toRadians, pow, round, Pi}

/**
  * 2nd milestone: basic visualization
  */
object Visualization extends VisualizationInterface {

  val ColorScale: Seq[(Temperature, Color)] =
    (Tuple2(60.0D, Color(255, 255, 255))::(32.0D, Color(255,0,0)) ::
      (12.0D, Color(255, 255, 0)) ::(0.0D, Color(0,255,255)) ::
      (-15.0D, Color(0,0,255)) :: (-27.0D, Color(255,0,255)) ::
      (-50.0D, Color(33, 0, 107)) :: (-60.0D, Color(0, 0, 0)) :: Nil).reverse

  /**
    * @param temperatures Known temperatures: pairs containing a location and the temperature at this location
    * @param location Location where to predict the temperature
    * @return The predicted temperature at `location`
    */
  def predictTemperature(temperatures: Iterable[(Location, Temperature)], location: Location): Temperature = {

    val p = 6
    inverseDistanceWeighting(temperatures, location, p)
  }

  /**
    * Inverted distance formula from
    * https://en.wikipedia.org/wiki/Inverse_distance_weighting
    * @param n
    * @param x
    * @param power
    * @return a predicted temperature given a set of intervals
    */
  private def inverseDistanceWeighting(n: Iterable[(Location, Temperature)],
                                       x: Location, power: Double): Temperature = {
    val distancesU = n.par.map{
      case (x_i, u_i) => (greatCircleDistance(x, x_i), u_i)
    }

    val closeDistances = distancesU.filter(_._1 < 1.0)
    if (closeDistances.nonEmpty) closeDistances.minBy(_._1)._2
    else {

      val wU = distancesU.map {
        case (distance_i, u_i) =>
          (1.0 / pow(distance_i, power), u_i)
      }

      def seqOp(a: (Double, Double), b: (Double, Double)): (Double,Double) = (a,b) match  {
        case ((acc1, acc2), (w_i, u_i)) =>
          (acc1 + w_i * u_i, acc2 + w_i)
      }
      def combOp(a: (Double,Double), b: (Double,Double)):(Double,Double) = (a,b) match {
        case ((wU1, w1), (wU2, w2)) => (wU1 + wU2, w1 + w2)
      }

      val temperatureSummation = wU
        .aggregate((0.0D, 0.0D)) (seqOp, combOp)

      val predictedTemperature = temperatureSummation._1/temperatureSummation._2
      predictedTemperature
    }
  }

  /**
    * @param points Pairs containing a value and its associated color
    * @param value The value to interpolate
    * @return The color that corresponds to `value`, according to the color scale defined by `points`
    */
  def interpolateColor(points: Iterable[(Temperature, Color)], value: Temperature): Color = {

    val sortedPoints = points.toSeq.sortBy(_._1)
    val pointInRefValues: Iterable[(Temperature, Color)] =
      sortedPoints.filter{case (temperature, _) => temperature == value}

    if (pointInRefValues.nonEmpty) pointInRefValues.iterator.next._2
    else {

      val refTemperatures = (Double.MinValue, Color(0,0,0)) +:
        sortedPoints :+
        (Double.MaxValue, Color(255, 255, 255))

      val intervals = (refTemperatures.init zip refTemperatures.tail)
        .map(interval =>
          TemperatureColorInterval(
            startTemperature = interval._1._1,
            endTemperature = interval._2._1,
            startColor = interval._1._2,
            endColor = interval._2._2
          )
        )

      val valueInterval = intervals.filter(interval =>
        interval.startTemperature < value && value < interval.endTemperature).head

      if (valueInterval.endTemperature == -60.0D) sortedPoints.head._2
      else if (valueInterval.startTemperature == 60.0D) sortedPoints.last._2
      else {
        interpolateForInterval(valueInterval, value)
      }
    }

  }

  private def sum(v1: Vector[Double], v2: Vector[Double]): Vector[Double] = {
    require(v1.length == v2.length)
    (v1 zip v2) map { case (x1, x2) => x1 + x2 }
  }

  /**
    * Linear interpolation formula taken from
    * https://en.wikipedia.org/wiki/Linear_interpolation
    * @param interval
    * @param value
    * @return The interpolated color according to the given temperature intervals
    */
  private def interpolateForInterval(interval: TemperatureColorInterval,
                                     value: Temperature): Color = {

    val y_0 = interval.startColor
    val y_1 = interval.endColor

    val x_0 = interval.startTemperature
    val x_1 = interval.endTemperature

    val x = value

    val y = sum(
      y_0 * (1.0D - (x-x_0)/(x_1-x_0)),
      y_1 * ((x - x_0)/(x_1 - x_0))
    ).map(round(_).toInt)

    Color(y(0), y(1), y(2))
  }

  /**
    * Transforms a given i,j matrix point to its corresponding GPS coordinate on a
    * 180*360 pixel image
    * @param height
    * @param width
    * @param i
    * @param j
    * @return The corresponding GPS  coordinate given an i,j location in a matrix
    */
  private def toGPSCoordinate(height: Int, width: Int, i: Int, j: Int): Location =
    Location((-i + (height +1)/2)*(180/height),
      (j - (width + 1)/2)*(360/width)
    )

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @return A 360×180 image where each pixel shows the predicted temperature at its location
    */
  def visualize(temperatures: Iterable[(Location, Temperature)], colors: Iterable[(Temperature, Color)]): Image = {

    val height = 180
    val width = 360
    val defaultAlpha = 255

    val pixels = (for {
      i <- (0 until height).par
      j <- 0 until width
    } yield {
      val location = toGPSCoordinate(height, width, i, j)
      val predictedTemperature = predictTemperature(temperatures, location)
      val color = interpolateColor(colors, predictedTemperature)
      Pixel(color.red, color.green, color.blue, defaultAlpha)
    }).toArray

    Image(width, height, pixels)
  }

  private def deltaOfSigma(location1: Location, location2: Location): Double = {

    if (location1 == location2) 0.0D
    else if (location1 isAntipodeOf location2) Pi
    else {
      val lon1Rad = toRadians(location1.lon)
      val lon2Rad = toRadians(location2.lon)
      val lat1Rad = toRadians(location1.lat)
      val lat2Rad = toRadians(location2.lat)

      val evalExpr = sin(lat1Rad) * sin(lat2Rad) + cos(lat1Rad) * cos(lat2Rad) * cos(abs(lon1Rad - lon2Rad))
      // In theory 'evalExpr' never gives a number less than = -1 or greater than 1. However, due to computation
      // errors, the result is sometimes -1.000000002 or 1.0000000002
      if (evalExpr < -1.0D)  Pi
      else if (evalExpr > 1.0D) 0.0D
      else acos(evalExpr)
    }
  }

  /**
    * Great circle distance taken from
    * https://en.wikipedia.org/wiki/Great-circle_distance
    * @param location1
    * @param location2
    * @return the corresponding circle distance between two locations
    */
  def greatCircleDistance(location1: Location, location2: Location): Double = {
    val earthRadiusKm = 6371.0D
    earthRadiusKm * deltaOfSigma(location1, location2)
  }

}

