package observatory

import com.sksamuel.scrimage.{Image, Pixel}

import math.{Pi, atan, sinh, toDegrees}

/**
  * 3rd milestone: interactive visualization
  */
object Interaction extends InteractionInterface {

  val DefaultAlpha: Int = 127

  /**
    * A tile is a 256x256 pixel image. Hence, each pixel is a tile with zoom 8 (pow(2,8) = 256)
    */
  val ZoomPixelLevel: Int = 8

  private def pow2(zoom: Int): Int = 1 << zoom

  /**
    * @param tile Tile coordinatesultAlpha
    * @return The latitude and longitude of the top-left corner of the tile, as per http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames
    */
  def tileLocation(tile: Tile): Location = {

    def lon(tile: Tile): Double = tile.x.toDouble/pow2(tile.zoom)*360.0D - 180.0D
    def lat(tile: Tile): Double = toDegrees(
      atan(
        sinh(
          Pi*(1.0 - 2.0*(tile.y.toDouble/pow2(tile.zoom)))
        )
      )
    )
    Location(lat(tile), lon(tile))
  }

  /**
    * @param temperatures Known temperatures
    * @param colors Color scale
    * @param tile Tile coordinates
    * @return A 256×256 image showing the contents of the given tile
    */
  def tile(temperatures: Iterable[(Location, Temperature)],
           colors: Iterable[(Temperature, Color)],
           tile: Tile): Image = {

    // To optimize the Visualization.interpolateColors method
    val sortedColors = colors.toSeq.sortBy(_._1)
    
    val x = tile.x * pow2(ZoomPixelLevel)
    val y = tile.y * pow2(ZoomPixelLevel)
    val zoom = tile.zoom

    val height = 256
    val width = 256

    val pixels = (for {
      i <- (0 until height).par
      j <- 0 until width
    } yield {
      val pixelTile = Tile(x + j, y + i, zoom + ZoomPixelLevel)
      val location = tileLocation(pixelTile)
      val predictedTemperature = Visualization.predictTemperature(temperatures, location)
      val color = Visualization.interpolateColor(
        sortedColors, predictedTemperature
      )

      Pixel(color.red, color.green, color.blue, DefaultAlpha)
    }).toArray

    Image(width, height, pixels)
  }

  /**
    * Generates all the tiles for zoom levels 0 to 3 (included), for all the given years.
    * @param yearlyData Sequence of (year, data), where `data` is some data associated with
    *                   `year`. The type of `data` can be anything.
    * @param generateImage Function that generates an image given a year, a zoom level, the x and
    *                      y coordinates of the tile and the data to build the image from
    */
  def generateTiles[Data](
    yearlyData: Iterable[(Year, Data)],
    generateImage: (Year, Tile, Data) => Unit
  ): Unit = {

    val zoomLevels = 0 to 3
    for {
      (year, data) <- yearlyData
      zoom <- zoomLevels
      x <- 0 until pow2(zoom)
      y <- 0 until pow2(zoom)
    } generateImage(year, Tile(x, y, zoom), data)
  }
}
