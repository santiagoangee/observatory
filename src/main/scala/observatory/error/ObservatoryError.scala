package observatory.error

import java.util.UUID

trait ObservatoryError extends Product with Serializable {
  val id: String = UUID.randomUUID().toString
}

object ObservatoryError {
  case class FileNotFoundError(filename: String)
      extends Throwable(s"filename $filename does not exist")
      with ObservatoryError

  case class AttributeNotFound(index: Int)
      extends Throwable(s"Attribute not found at index $index")
      with ObservatoryError

  case class EmptyAttribute(index: Int)
      extends Throwable(s"Empty attribute on index $index")
      with ObservatoryError

  case class InvalidAttributeType(attribute: String)
      extends Throwable(s"Invalid attribute type: $attribute")
      with ObservatoryError

  case object StreamFilterError
      extends Throwable("error while trying to clean empty or corrupted lines")
      with ObservatoryError
}
