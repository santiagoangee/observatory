package observatory

object Main extends App {
  val yearlyAverage1975 = Extraction.locateTemperatures(1975, "/stations.csv", "/1975.csv")
  val yearlyAverageRecords1975: Iterable[(Location, Temperature)] =
    Extraction.locationYearlyAverageRecords(yearlyAverage1975)

  val yearlyAverage1976 = Extraction.locateTemperatures(1976, "/stations.csv", "/1976.csv")
  val yearlyAverageRecords1976: Iterable[(Location, Temperature)] =
    Extraction.locationYearlyAverageRecords(yearlyAverage1976)

  val average = Manipulation.average(Iterable(yearlyAverageRecords1975, yearlyAverageRecords1976))

  val deviations1975 = Manipulation.deviation(yearlyAverageRecords1975, average)

  (Grid.MaxLatitude to Grid.MinLatitude by Grid.DecreasingOrder) foreach {
    lat => {
      (Grid.MinLongitude to Grid.MaxLongitude)
        .foreach (lon => {
          val gridLocation = GridLocation(lat, lon)
          val deviation = deviations1975(gridLocation)
          print(s"$deviation ")
        });
      println
    }
  }

  println("finish")
}
