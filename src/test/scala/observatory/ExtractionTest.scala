package observatory

import org.scalatest.funsuite.AnyFunSuite

import java.time.LocalDate

import org.apache.spark.rdd.RDD

import Extraction.spark
import Extraction.StationsStruct
import Extraction.TemperaturesStruct
import Extraction.sparkTemperatures
import Extraction.sparkAverageRecords
import Extraction.fahrenheitToCelsius

trait ExtractionTest extends AnyFunSuite with MilestoneSuite {
  private val milestoneTest = namedMilestoneTest("data extraction", 1) _

  import spark.implicits._
  // Implement tests for the methods of the `Extraction` object
  test("The program should filter single StnIDs and WbanIDs") {
    val testYear = 1975
    val inputStationsRDD: RDD[String]  =
      spark.sparkContext.parallelize(
        Array(",,1.0,2.0",
          "1,,3.0,4.0",
          ",2,10.0,100.00",
          "3,4,23.0,24.0",
          ",,2.0,3.0"))

    val inputTemperaturesRDD: RDD[String] =
      spark.sparkContext.parallelize(
        Array("1,,1,1,2.0",
          "1,,3,24,4.0",
          ",2,1,1,-40.0",
          "3,4,12,12,-5.0"))

    val testStationsDataFrame = spark.read
      .option("sep", ",")
      .schema(StationsStruct)
      .csv(inputStationsRDD.toDS)

    val testTemperaturesDataFrame = spark.read
      .option("sep", ",")
      .schema(TemperaturesStruct)
      .csv(inputTemperaturesRDD.toDS)

    val givenSparkTemperatures = sparkTemperatures(testYear,
      testStationsDataFrame, testTemperaturesDataFrame).collect
    val expectedSparkTemperatures = Seq(
      (LocalDate.of(testYear, 12, 12), Location(23.0D, 24.0D), fahrenheitToCelsius(-5.0D)))
    val res = givenSparkTemperatures(1) == expectedSparkTemperatures.head
    assert(res)
  }

}