# Functional Programming with Scala Capstone

This is the capstone project of the Coursera specialization "Functional programming with Scala" offered by the
École Polytechnique Fédérale de Lausanne. More information can be found at https://www.coursera.org/learn/scala-capstone